package cn.jbolt.api.common.controller;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.jfinal.kit.JsonKit;

import java.util.HashMap;
import java.util.Map;

/**
 * 微信小程序获取小程序码工具类
 *
 * @ClassName: WxXcxUtil
 * @author: xiaofu QQ：593391262
 * @date: 2022年7月25日
 */
public class WxXcxUtil {

    /**
     * 1，第一步 获取 accessToken
     * 2，**appId,和appSecret是小程序的**，不是公众号的，注意
     *
     * @return
     */
    public static String getAccessToken(String appId, String appSecret) {
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + appSecret;
        String data = HttpUtil.get(url);
        JSONObject jsonObject = JSONUtil.parseObj(data);
        String accessToken = "";
        if (String.valueOf(jsonObject.get("expires_in")).equals("7200")) {
            accessToken = String.valueOf(jsonObject.get("access_token"));
        }
        return accessToken;
    }

    /**
     * 2，第一步 根据token获取二维码
     *
     * @param accessToken
     * @return
     */
    public static byte[] getActivityQrCodeByAccessToken(String accessToken, String scene, String page)  {
        /********* 封装请求参数 **********/
        Map<String, Object> paraMap = new HashMap<>();
        //二维码携带参数 不超过32位 参数类型必须是字符串
        paraMap.put("scene", scene);
        //二维码跳转页面
        paraMap.put("page", page);
        //二维码的宽度
        paraMap.put("width", 250);
        //自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调
        paraMap.put("auto_color", false);
        //是否需要透明底色， is_hyaline 为true时，生成透明底色的小程序码
        paraMap.put("is_hyaline", false);
        //要打开的小程序版本。正式版为 "release"，体验版为 "trial"，开发版为 "develop"
        paraMap.put("env_version", "develop");
        //检查page 是否存在，为 true 时 page 必须是已经发布的小程序存在的页面（否则报错）；为 false 时允许小程序未发布或者 page 不存在， 但page 有数量上限（60000个）请勿滥用
        paraMap.put("check_path", false);

        /********* 执行post请求微信 获取二维码数据流 **********/
        byte[] result = getQrCodeByParam("https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" + accessToken, paraMap);
        return result;
    }


    /**
     * 一物一码
     * 获取小程序二维码数据流
     *
     * @param url     请求路径
     * @param paraMap 其他参数
     * @return
     */
    public static byte[] getQrCodeByParam(String url, Map<String, Object> paraMap) {
        HttpRequest httpPost = HttpUtil.createPost(url);
        httpPost.header("Content-Type", "application/json");
        // 设置请求的参数
        httpPost.body(JsonKit.toJson(paraMap), "UTF-8");
        HttpResponse response = httpPost.execute();
        return response.bodyBytes();
    }
}
