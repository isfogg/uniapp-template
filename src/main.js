/*
 * @Author: zy
 * @Date: 2022-06-07 19:14:16
 * @LastEditTime: 2022-07-26 23:35:43
 * @Description: 
 */
import Vue from 'vue'
import App from './App'
import uView from 'uview-ui'

import $store from './store'
import * as util from './utils/index'
import * as filters from './utils/filters'

import $api from './services/index'
import mixin from "./mixins/index";

import { router, RouterMount } from './routers/index'  //路径换成自己的
Vue.use(router);

Vue.use(uView)
Vue.mixin(mixin)

Vue.config.productionTip = false;

// 注册过滤器
Object.keys(filters).forEach(k => Vue.filter(k, filters[k]))

let mpShare = require("uview-ui/libs/mixin/mpShare.js");
Vue.mixin(mpShare);

Vue.prototype = Object.assign(Vue.prototype, {
  $store,
  $api,
  $xf: util
});

App.mpType = "app";
// 页面的 onLoad 在 onLaunch 之后执行
Vue.prototype.$onLaunched = new Promise(resolve => {
  Vue.prototype.$isResolve = resolve;
});
const app = new Vue({
  ...App
});

// #ifdef H5
RouterMount(app,router,'#app')
// #endif

// #ifndef H5
app.$mount() //为了兼容小程序及app端必须这样写才有效果
// #endif
