/*
 * @Author: zy
 * @Date: 2022-06-08 09:44:58
 * @LastEditTime: 2022-07-27 10:04:53
 * @Description:项目配置文件
 */
import platform from "@/utils/platform";

export const serviceEev = process.env.NODE_ENV;
//开发模式 true的时候有debug日志 上生产请用false
export const devMode = serviceEev === "production" ? false : true;
// API地址
const baseUrlConfig = {
  production: "https://uniapp.xiaofu.fun",
  test: "http://192.168.3.100",
  development: "/api"
};
export const baseURL = serviceEev
  ? baseUrlConfig[serviceEev]
  : baseUrlConfig["test"];
export const appVersion = "1.0";
export const appName = "小弗uniApp-template";
export const appPlatform = platform;
export const loginPage = "/pages/auth/login";

const defaultConfig = {
  baseURL: `${baseURL}`,
  appVersion: `${appVersion}`,
  appName: `${appName}`,
  appPlatform: `${appPlatform}`,
  serviceEev: `${serviceEev}`,
  devMode: `${devMode}`
};

console.log(
  `${appName} V:${appVersion} EVN:${serviceEev} P:${appPlatform} DEV:${devMode}`
);

export default defaultConfig;

export const bucketSn = "653678258";

//JBolt 极速开发平台 主配置文件
export const jbConfig = {
  ENABLE: true, //启用JBolt
  DEV_MODE: `${devMode}`, //开发模式 true的时候有debug日志 上生产请用false
  API_HOST: `${baseURL}`, //API访问HOST
  IMG_HOST: `${baseURL}`, //图片访问HOST
  APPID: "jb5za6ghb69solt", //JBolt平台分配的应用APPID
  JBOLT_API_REQUEST: "JBOLTAPI", //标识是JBoltSdk访问API 默认就好
  JBOLT_APPID_KEY: "jboltappid", //header中的appid参数名 默认就好
  JBOLT_JWT_KEY: "jboltjwt", //header中的jwt参数名 默认就好
  JBOLT_REFRESH_JWT_KEY: "jboltrefreshjwt", //header中的refreshjwt参数名 默认就好
  JBOLT_JWT_SIGNATURE_KEY: "jboltsignature", //header中的sign参数名 默认就好
  JBOLT_JWT_TIMESTAMP_KEY: "jbolttimestamp", //header中的时间戳参数名 默认就好
  JBOLT_JWT_NONCE_KEY: "jboltnonce", //header中的随机数参数名 默认就好
  JBOLT_GOTOLOGIN_TYPE: 1, //1:wxLogin or refreshJwtApi,2:跳转到Login界面
  JBOLT_GOTOLOGIN_PAGE_URL: "/pages/jbolt/user/login/index",
  JBOLT_JWT_AUTO_REFRESH: true, //是否启用session过期自动请求新JWT
  JBOLT_JWT_CHECKSIGN: false, //是否启用checksign 跟JBolt平台应用设置保持一致
  jwt: null, //通行证TOKEN JWT 需要调动接口获取 不是直接配置
  refreshJwt: null, //通行证TOKEN JWT 需要调动接口获取 不是直接配置
  REFRESH_JWT_TYPE: "api" //默认刷新jwt的方式 默认使用wx.login 可配置 wxlogin和api两种
};

// 本地存储
export const localStorageKey = {
  sessionKey: "session_id",
  sessionKeyExpireTime: "session_id_expire_time",
  token: "token",
  tokenExpireTime: "token_expire_time",
  refreshToken: "refresh_token",
  refreshTokenExpireTime: "refresh_token_expire_time",
  memberId: "member_id"
};
