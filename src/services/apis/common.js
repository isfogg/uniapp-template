/*
 * @Author: zy
 * @Date: 2022-06-07 20:03:29
 * @LastEditTime: 2022-07-26 18:17:57
 * @Description:通用相关接口
 */
import { fetch } from "../fetch";
import $uploadFile from "../upload";

export default {
  getQiniuToken(extension, fileKey, bucketSn) {
    return fetch({
      api: "/api/xf/common/getQiniuToken",
      params: { extension: extension, fileKey: fileKey, bucketSn: bucketSn },
      loading: false,
      auth: false,
      error: true
    });
  },
  uploadFile(filePath, params) {
    return $uploadFile(Object.assign({ filePath }, { formData: params }));
  },
  /**
   * 封装文件自动上传七牛云
   * @param {string} extension 文件后缀
   * @param {string} fileKey 七牛云key
   * @param {string} bucketSn 后台配置sn
   * @param {string} file
   * @returns
   */
  autoUploadQiniu(extension, fileKey, bucketSn, file) {
    let that = this;
    return new Promise((resolve, reject) => {
      that.getQiniuToken(extension, fileKey, bucketSn).then(
        tokenRes => {
          that.uploadFile(file, tokenRes).then(
            uploadRes => {
              console.log(uploadRes);
              resolve(tokenRes.file_url);
            },
            uploadRrr => {
              reject(uploadRrr);
            }
          );
        },
        tokenRrr => {
          reject(tokenRrr);
        }
      );
    });
  },
  // 查询数据字典
  getPoptions(params, loading = false) {
    return fetch({
      api: "/api/xf/common/poptions",
      params,
      loading: false,
      auth: false
    });
  }, // 应用信息
  getAppInfo(appId, loading = true) {
    return fetch({
      api: "/api/xf/common/appInfo",
      loading: loading,
      auth: false,
      params: { appId: appId }
    });
  },
  // 系统用户列表
  getAdminUserList(params, loading = false) {
    return fetch({
      api: "/api/xf/common/getAdminUserList",
      loading: loading,
      auth: false,
      params
    });
  }
};
