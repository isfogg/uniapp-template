/*
 * @Author: zy
 * @Date: 2022-07-24 22:51:41
 * @LastEditTime: 2022-07-26 22:27:51
 * @Description: 
 */
/**
 * JBolt 极速开发平台 主配置文件
 */
import { jbConfig } from "@/config/index";
const config = jbConfig;
module.exports = {
    config: config
};
