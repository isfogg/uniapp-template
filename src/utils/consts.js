/*
 * @Author: zy
 * @Date: 2022-07-24 22:51:41
 * @LastEditTime: 2022-07-26 16:22:12
 * @Description: 常量定义
 */
export default {};

// 性别
export const genderStatus = {
  male: 1,
  female: 2,
  secrecy: 0
};
