/*
 * @Author: zy
 * @Date: 2022-06-12 10:53:28
 * @LastEditTime: 2022-07-24 23:27:44
 * @Description:用户相关路由
 */
export default [
  {
    path: "/pages/user/index"
  },
  {
    path: "/pages/user/contact"
  },
  {
    path: "/pages/user/feedback"
  },
  {
    path: "/pages/user/info"
  }
];
