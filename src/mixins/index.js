/*
 * @Author: zy
 * @Date: 2022-07-24 22:51:41
 * @LastEditTime: 2022-07-27 11:35:30
 * @Description:公用方法
 */
import { loginPage } from "@/config";

export default {
  async onLoad() {
    this.hasReady = true;
  },
  created() {
    this.hasReady = true;
  },
  data() {
    return {
      hasReady: false,
    };
  },
  computed: {
    hasLogin() {
      if (this.hasReady) {
        return this.$store.getters.hasLogin;
      }
      return;
    },
    defaultAppInfo() {
      if (this.hasReady) {
        return this.$store.getters.defaultAppInfo;
      }
    },
  },
  watch: {
    async hasLogin(val) {
      // 登录成功
      console.log("登录完毕:", val);
      if (val) {
        if (this.onLogin) {
          this.onLogin(this.hasLogin);
        }
      }
    },
  },
  methods: {
    goPage(path, isLoginPage = false) {
      if (path) {
        if (isLoginPage && !this.hasLogin) {
          console.log("未登录 跳转登录页");
          this.goLogin();
          return;
        }
        if (getApp().globalData.isGoPage) {
          getApp().globalData.isGoPage = false;
          uni.navigateTo({
            url: path,
          });
          setTimeout(() => {
            getApp().globalData.isGoPage = true;
          }, 200);
        } else {
          // console.log('重复点击了')
        }
      } else {
        uni.showToast({
          title: "暂无页面",
          icon: "none",
          duration: 2000,
        });
      }
    },
    goLogin() {
      uni.navigateTo({
        url: loginPage,
      });
    },
    newToast(content) {
      this.$xf.toast(content);
    },
  },
};
