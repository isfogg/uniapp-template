/*
 * @Author: zy
 * @Date: 2022-06-07 19:14:18
 * @LastEditTime: 2022-07-26 16:26:26
 * @Description: defaultAppInfo
 */
import commonApis from "@/services/apis/common";

export default {
  state: {
    defaultAppInfo: null
  },
  actions: {
    async getDefaultAppInfo({ state, dispatch }) {
      if (!state.defaultAppInfo) {
        state.defaultAppInfo = await commonApis.getAppInfo();
        state.defaultAppInfo.appLogo = "/static/logo.png";
        state.defaultAppInfo.aboutUrl = "https://www.xiaofu.fun/";
      }
      return state.defaultAppInfo;
    }
  },
  getters: {
    defaultAppInfo: state => state.defaultAppInfo
  }
};
